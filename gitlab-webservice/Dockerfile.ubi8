## FINAL IMAGE ##

ARG RAILS_IMAGE=

FROM ${RAILS_IMAGE}

ARG GITLAB_CONFIG=/srv/gitlab/config
ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-webservice" \
      name="GitLab Web Service" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="GitLab Web Service runs the GitLab Rails application with Puma web server." \
      description="GitLab Web Service runs the GitLab Rails application with Puma web server."

ADD gitlab-webservice-ee.tar.gz /
ADD gitlab-python.tar.gz /
ADD gitlab-logger.tar.gz /usr/local/bin

COPY scripts/ /scripts
COPY --chown=${UID}:0 configuration/ ${GITLAB_CONFIG}/

ENV GITALY_FEATURE_DEFAULT_ON=1

RUN dnf ${DNF_OPTS} install -by --nodocs procps \
    && dnf clean all \
    && rm -r /var/cache/dnf \
    && cd /srv/gitlab \
    && mkdir -p public/uploads \
    && chmod 0700 public/uploads \
    && chown -R ${UID}:0 public/uploads /home/${GITLAB_USER} /var/log/gitlab \
    && chmod -R g=u public/uploads /home/${GITLAB_USER} /var/log/gitlab

USER ${UID}

# Declare /var/log volume after initial log files
# are written to the perms can be fixed
VOLUME /var/log

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck

CMD ["/scripts/process-wrapper"]
