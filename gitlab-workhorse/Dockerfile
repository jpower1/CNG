ARG CI_REGISTRY_IMAGE="registry.gitlab.com/gitlab-org/build/cng"
ARG TAG="master"

ARG GO_TAG="master"
ARG GITLAB_EDITION=gitlab-rails-ee
ARG RAILS_VERSION="master"

ARG GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${TAG}"

ARG EXIFTOOL_IMAGE="$CI_REGISTRY_IMAGE/gitlab-exiftool:${EXIFTOOL_VERSION}"

FROM ${CI_REGISTRY_IMAGE}/${GITLAB_EDITION}:${RAILS_VERSION} as rails
FROM ${EXIFTOOL_IMAGE} as exiftool_image

FROM ${CI_REGISTRY_IMAGE}/gitlab-go:${GO_TAG} as builder

ARG BUILD_DIR=/tmp/build
RUN mkdir -p ${BUILD_DIR}
COPY --from=rails /srv/gitlab/ ${BUILD_DIR}/

RUN buildDeps=' \
  git make' \
  && apt-get update \
  && apt-get install -y --no-install-recommends $buildDeps \
  && make -C ${BUILD_DIR}/workhorse install \
  && rm -rf ${BUILD_DIR} \
  && apt-get purge -y --auto-remove $buildDeps \
  # install perl for exiftool requirement
  && apt-get install -y --no-install-recommends perl \
  && rm -rf /var/lib/apt/lists/*

## FINAL IMAGE ##

FROM ${GITLAB_BASE_IMAGE} as final

ARG DATADIR=/var/opt/gitlab
ARG CONFIG=/srv/gitlab/config
ARG GITLAB_USER=git

# create gitlab user
RUN adduser --disabled-password --gecos 'GitLab' ${GITLAB_USER} && \
  install -d -o ${GITLAB_USER} /var/log/gitlab/ && \
  install -d -o ${GITLAB_USER} ${DATADIR} && \
  install -d -o ${GITLAB_USER} ${CONFIG}

COPY --from=builder /usr/local/bin/gitlab-* /usr/local/bin/

COPY --from=rails --chown=git /srv/gitlab/public/ /srv/gitlab/public/

COPY --from=rails --chown=git /srv/gitlab/doc/ /srv/gitlab/doc/

# Requirement of exiftool
RUN apt-get update -y \
  && apt-get install --no-install-recommends -y perl \
  && rm -rf /var/lib/apt/lists/* 

COPY --from=exiftool_image / /

USER $GITLAB_USER:$GITLAB_USER

COPY scripts/ /scripts/

CMD ["/scripts/start-workhorse"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 \
CMD /scripts/healthcheck
